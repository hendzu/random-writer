import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;

import org.junit.Test;

public class RandomWriterTest {

	@Test
	public void noCrash() throws Exception {
		String[] arguments1 = { "3", "100", "source.txt", "result.txt" };
		RandomWriter.main(arguments1);

	}

	@Test
	public void noCrash2() throws Exception {
		String[] arguments2 = { "3", "20", "source.txt", "result2.txt" };
		RandomWriter.main(arguments2);
	}

	@Test
	public void noCrash3() throws Exception {
		String[] arguments3 = { "5", "100", "source2.txt", "result3.txt" };
		RandomWriter.main(arguments3);
	}

	@Test
	public void rightLength() throws Exception {
		String[] arguments4 = { "7", "10000", "source3.txt", "result4.txt" };
		RandomWriter.main(arguments4);
		BufferedReader textfile = new BufferedReader(new FileReader(
				"result4.txt"));
		String oneline;
		oneline = textfile.readLine();
		assertTrue(oneline.length() == 10000);
		textfile.close();
	}

}
