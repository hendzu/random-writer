import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class RandomWriter {

	private static int k; // seedi pikkus
	private static int length; // resulti pikkus
	private static StringBuffer result = new StringBuffer();
	private static ArrayList<Character> nextChars = new ArrayList<Character>();

	public static void setupArguments(String karg, String lengtharg) {
		k = Integer.parseInt(karg);
		length = Integer.parseInt(lengtharg);
		if (k < 0 || length < 0) {
			throw new RuntimeException("k and/or length can't be negative!");
		} else if (k > length) {
			throw new RuntimeException("k can't be bigger than length!");
		}
	}

	public static StringBuffer readSource(String sourcepath) {
		try {
			BufferedReader textfile = new BufferedReader(new FileReader(
					sourcepath));
			String oneline;
			StringBuffer source = new StringBuffer();

			while ((oneline = textfile.readLine()) != null) {
				source.append(oneline);
				source.append(" ");//reavahetusel s�nade vahele t�hik
			}
			textfile.close();
			checkSourceLength(source);

			return source;

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void checkSourceLength(StringBuffer source) {
		if (source.length() < k) {
			throw new RuntimeException("Text isn't long enough for k.");
		}
	}

	public static StringBuffer findSeed(StringBuffer source) {
		//leiab mingi suvalise s�na juppi pikkusega k ja kirjutab selle resulti
		//(v�i nii pikkalt kui mahub).

		int randomnumber = (int) Math.round(Math.random()
				* (source.length() - k));
		StringBuffer seed = new StringBuffer(source.substring(randomnumber,
				randomnumber + k));
		if ((length - result.length()) < k) {
			result.append(seed.substring(0, length - result.length()));
		} else {
			result.append(seed.toString());
		}
		return seed;
	}

	public static StringBuffer nextSeedChoices(StringBuffer source,
			StringBuffer seed) {
		int nextIndex = source.indexOf(seed.toString()) + k;

		// kui nextIndex asub teksti l�pus, siis leiab uue seedi ja j�tkab
		if ((nextIndex + k) > source.length()) {
			seed = findSeed(source);
		} else {
			nextChars.add(source.substring(nextIndex).charAt(0));
		}
		//leiab k�ik otse peale seedi esinemist esinevad t�hed
		while ((nextIndex + k) < source.length()) {
			nextIndex = source.indexOf(seed.toString(), nextIndex + 1);
			if (nextIndex == -1) {//kui rohkem ei leidu v�ljub
				break;
			} else if ((nextIndex + k) >= source.length()) {
				//kui seed asub juttu l�pus v�tab uue seedi
				seed = findSeed(source);
			} else {
				nextChars.add((source.substring(nextIndex + k).charAt(0)));
			}
		}
		return seed;
	}

	public static StringBuffer nextSeed(StringBuffer seed) {
		//lisab v�ljundisse ja seedi l�ppu loendist suvalise t�he ning kustutab seedi esimese t�he
		if (nextChars.size() > 0) {
			int randomnumber = (int) Math.round(Math.random()
					* (nextChars.size() - 1));
			Character randomchar = nextChars.get(randomnumber);
			result.append(randomchar);
			seed.append(randomchar);
			seed.deleteCharAt(0);
		}
		return seed;
	}

	public static StringBuffer getResult(StringBuffer source, StringBuffer seed) {
		while (result.length() < length) {
			seed = nextSeed(nextSeedChoices(source, seed));
			nextChars.clear();
		}
		return result;
	}

	public static void writeResult(String resultpath, StringBuffer result) {
		try {
			FileWriter textfile = new FileWriter(resultpath);
			textfile.write(result.toString());
			result.delete(0, result.length());//muidu j��b j�rgmisel korral sisse
			textfile.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	public static void main(String[] args) throws Exception {
		// four command line arguments - k, length, source, result
		setupArguments(args[0], args[1]);//paneb k ja length globaalseteks muutujateks
		StringBuffer source = new StringBuffer(readSource(args[2]));
		StringBuffer seed = new StringBuffer(findSeed(source));
		writeResult(args[3], getResult(source, seed));
	}
}
